import org.junit.*;
import static org.junit.Assert.assertEquals;

import java.beans.Transient;

public class TestWeekEnd {
    private Personne pierre;
    private Personne paul;
    private Personne vincent;
    private Personne anais;
    private Personne benjamin;
    
    private Depense depense1;
    private Depense depense2;
    private Depense depense3;
    private Depense depense4;
    private Depense depense5;

    private WeekEnd week1;

    @Before
    public void initialisationPersonne(){
        this.pierre = new Personne("Pierre");
        this.paul = new Personne("Paul");
        this.vincent = new Personne("Vincent");
        this.anais = new Personne("Anais");
        this.benjamin = new Personne("Benjamin");
    }
    @Before
    public void initialisationDepense(){
        this.depense1 = new Depense("pain", 12, pierre);
        this.depense2 = new Depense("pizzas", 100, paul);
        this.depense3 = new Depense("essence", 70, pierre);
        this.depense4 = new Depense("pain", 12, vincent);
        this.depense5 = new Depense("pain", 6, vincent);
    }
    @Before
    public void initialisationWeek(){
        this.week1 = new WeekEnd();
    }

    @Test
    public void testAjouterDepense(){
        week1.ajouterDepense(depense1);
        assertEquals(12, week1.totalDepenses());
    }

    @Test
    public void testTotalDepense(){
        week1.ajouterDepense(depense1);
        week1.ajouterDepense(depense2);
        week1.ajouterDepense(depense3);
        week1.ajouterDepense(depense4);
        week1.ajouterDepense(depense5);
        assertEquals(200, week1.totalDepenses());
    }

    @Test
    public void testDepensePersonne(){
        week1.ajouterDepense(depense1);
        week1.ajouterDepense(depense3);
        week1.ajouterPersonne(pierre);
        assertEquals(82, week1.depenseParPersonne(pierre));
    }

    @Test
    public void testMoyenPersonne(){
        week1.ajouterDepense(depense1);
        week1.ajouterDepense(depense2);
        week1.ajouterPersonne(pierre);
        week1.ajouterPersonne(paul);
        assertEquals(56, week1.depenseMoyenne(), 0.1);
    }

    @Test
    public void testTotalDepenseProduit(){
        week1.ajouterDepense(depense4);
        week1.ajouterDepense(depense5);
        week1.ajouterPersonne(vincent);
        assertEquals(8, week1.totalDepenseProduit("pain"));
    }

    @Test
    public void testElegant1(){
        week1.ajouterPersonne(anais);
        week1.ajouterPersonne(benjamin);
        assertEquals(avoirPersonne(anais), avoirPersonne(benjamin));
    }

    @Test
    public void testElegant2(){
        week1.ajouterDepense(depense4);
        week1.ajouterDepense(depense5);
        week1.ajouterPersonne(vincent);
        week1.ajouterPersonne(anais);
        assertTrue(avoirPersonne(anais)<avoirPersonne(vincent));
    }
    @Test
    public void testElegant3(){
        week1.ajouterDepense(depense4);
        week1.ajouterDepense(depense5);
        week1.ajouterPersonne(vincent);
        assertEquals(0, avoirPersonne(vincent));
    }
    @Test
    public void testElegant4(){
        week1.ajouterDepense(depense1);
        week1.ajouterDepense(depense3);
        week1.ajouterDepense(depense4);
        week1.ajouterDepense(depense5);
        week1.ajouterPersonne(vincent);
        week1.ajouterPersonne(pierre);
        assertEquals(0, avoirPersonne(vincent) + avoirPersonne(pierre));
    }
}