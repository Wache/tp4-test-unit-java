import java.util.ArrayList;
import java.util.List;

public class WeekEnd {
    private List<Personne> listeAmis;
    private List<Depense> listeDepenses;

    public WeekEnd() {
        this.listeAmis = new ArrayList<>();
        this.listeDepenses = new ArrayList<>();
    }

    public void ajouterPersonne(Personne personne) {
        this.listeAmis.add(personne);
    }

    public void ajouterDepense(Depense dpense) {
        this.listeDepenses.add(dpense);
    }

    // totalDepensesPersonne prend paramètre une personne
    // et renvoie la somme des dépenses de cette personne.
    public int depenseParPersonne(Personne personne) {
        int total = 0;
        for (Depense dep : this.listeDepenses) {
            if (dep.getQui().equals(personne)) {
                total += dep.getMontant();
            }
        }
        return total;
    }

    // totalDepenses renvoie la somme de toutes les dépenses.
    public int totalDepenses() {
        int total = 0;
        for (Depense dep : this.listeDepenses) {
            total += dep.getMontant();
        }
        return total;
    }

    // depenseMoyenne renvoie la moyenne des dépenses par
    // personne
    public double depenseMoyenne() {
        return totalDepenses() / this.listeAmis.size();
    }

    // totalDepenseProduit prend un nom de produit en paramètre et renvoie la
    // somme des dépenses pour ce produit.
    // (du pain peut avoir été acheté plusieurs fois...)
    public int totalDepenseProduit(String nomProduit) {
        int total = 0;
        for (Depense dep : this.listeDepenses) {
            if (dep.getProduit().equals(nomProduit)) {
                total += dep.getMontant();
            }
        }
        return total;
    }

    // avoirParPersonne prend en paramètre une personne et renvoie
    // son avoir pour le week end.
    public double avoirPersonne(Personne personne) {
        return depenseMoyenne() - depenseParPersonne(personne);
    }
}