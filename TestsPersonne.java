import org.junit.*;
import static org.junit.Assert.assertEquals;

public class TestsPersonne {

    @Test
    public void testGetPrenom(){
        Personne jean = new Personne("Jean");
        assertEquals("Jean", jean.getPrenom());
    }
}