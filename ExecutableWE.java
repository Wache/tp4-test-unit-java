public class ExecutableWE {
    public static void main(String [] arg) {
        // Création des personnes;
        Personne Pierre = new Personne("Pierre");
        Personne Paul = new Personne("Paul");
        Personne Marie = new Personne("Marie");
        Personne Anna = new Personne("Anna");
        
        // Création des depenses;
        Depense depense1 = new Depense("pain", 12, Pierre);
        Depense depense2 = new Depense("pizzas", 100, Paul);
        Depense depense3 = new Depense("essence", 70, Pierre);
        Depense depense4 = new Depense("vin", 15, Marie);
        Depense depense5 = new Depense("vin", 10, Paul);

        // Création d'un weekend;
        WeekEnd unWeekEnd = new WeekEnd();

        // Ajout des dépenses dans la liste des dépenses;
        unWeekEnd.ajouterDepense(depense1);
        unWeekEnd.ajouterDepense(depense2);
        unWeekEnd.ajouterDepense(depense3);
        unWeekEnd.ajouterDepense(depense4);
        unWeekEnd.ajouterDepense(depense5);

        // Ajout des personnes dans la liste des personnes;
        unWeekEnd.ajouterPersonne(Pierre);
        unWeekEnd.ajouterPersonne(Paul);
        unWeekEnd.ajouterPersonne(Marie);
        unWeekEnd.ajouterPersonne(Anna);

        System.out.println(unWeekEnd.totalDepenses()); // 207
        System.out.println(unWeekEnd.depenseParPersonne(Pierre)); //82
        System.out.println(unWeekEnd.depenseParPersonne(Anna)); // 0
        System.out.println(unWeekEnd.depenseMoyenne()); // 51.0
        System.out.println(unWeekEnd.totalDepenseProduit("vin")); // 25
        System.out.println(unWeekEnd.avoirPersonne(Paul)); // -59.0
    }
}