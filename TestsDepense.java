import org.junit.*;
import static org.junit.Assert.assertEquals;




public class TestsDepense {
    @Test
    public void testGetProduit(){
        Depense pain = new Depense("Pain");
        assertEquals("Pain", pain.getProduit());
    }
    @Test
    public void testGetMontant(){
        Depense 12 = new Depense(12);
        assertEquals(12, 12.getMontant());
    }
    @Test
    public void testGetQui(){
        Depense pierre = new Depense("Pierre");
        assertEquals("Pierre", pierre.getQui());
    }
}
